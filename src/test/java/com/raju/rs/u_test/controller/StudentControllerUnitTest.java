package com.raju.rs.u_test.controller;

import static com.raju.rs.util.CommonUtil.asJsonString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.raju.rs.controller.StudentController;
import com.raju.rs.entity.Student;
import com.raju.rs.service.StudentService;
import com.raju.rs.util.TestDataHelper;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@WebMvcTest(StudentController.class)
public class StudentControllerUnitTest {

	@MockBean
	private StudentService studentService;

	@Autowired
	private MockMvc mockMvc;

	private TestDataHelper testDateHelper;

	@BeforeEach
	private void setup() {
		testDateHelper = new TestDataHelper();
	}

	@Test
	@DisplayName("GET /student/1 - Found")
	void testGetStudentByIdFound() throws Exception {
		// Setup our mocked service
		Student mockStudent = testDateHelper.getStudentRajendra();
		mockStudent.setUserId(1L);
		doReturn(Optional.of(mockStudent)).when(studentService).findById(1L);

		// Execute the GET request
		mockMvc.perform(get("/student/{id}", 1))
				// Validate the response code and content type
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				// Validate the returned fields
				.andExpect(jsonPath("$.userId", equalTo(1))).andExpect(jsonPath("$.firstName", equalTo("Rajendra")))
				.andExpect(jsonPath("$.lastName", equalTo("Thorat")));
	}

	@Test
	@DisplayName("GET /student/1 - Not Found")
	void testGetStudentByIdNotFound() throws Exception {
		doReturn(Optional.empty()).when(studentService).findById(1000L);
		mockMvc.perform(get("/student/{id}", 1000L)).andExpect(status().isNotFound());
	}

	@Test
	@DisplayName("GET /student/ - Find All Found")
	void testGetAllStudentFound() throws Exception {

		List<Student> studentList = new ArrayList<>(4);
		studentList.add(testDateHelper.getStudentRajendra());
		studentList.add(testDateHelper.getStudentVivek());
		studentList.add(testDateHelper.getStudentPrashant());
		studentList.add(testDateHelper.getStudentPriti());

		doReturn(studentList).when(studentService).findAll();

		// Execute the GET request

		mockMvc.perform(get("/student"))
				// Validate the response code and content type
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				// Validate the returned fields
				.andExpect(jsonPath("$", hasSize(4))).andExpect(jsonPath("$[0].firstName", equalTo("Rajendra")))
				.andExpect(jsonPath("$[0].lastName", equalTo("Thorat")))
				.andExpect(jsonPath("$[3].firstName", equalTo("Priti")))
				.andExpect(jsonPath("$[3].lastName", equalTo("Tiwari"))).andDo(print());

	}

	@Test
	@DisplayName("GET /student/ - Find All Not Found")
	void testGetAllStudentNotFound() throws Exception {

		List<Student> studentList = new ArrayList<>(0);

		doReturn(studentList).when(studentService).findAll();

		// Execute the GET request
		mockMvc.perform(get("/student"))
				// Validate the response code and content type
				.andExpect(status().isNotFound());

	}

	@Test
	@DisplayName("POST /student")
	void testAddNewStudent() throws Exception {

		Student studentRajendra = testDateHelper.getStudentRajendra();
		studentRajendra.setUserId(1L);
		doReturn(studentRajendra).when(studentService).addNewStudent(any());

		mockMvc.perform(post("/student").contentType(MediaType.APPLICATION_JSON).content(asJsonString(studentRajendra)))

				// Validate the response code and content type
				.andExpect(status().isCreated()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))

				// Validate the returned fields
				.andExpect(jsonPath("$.userId", equalTo(1))).andExpect(jsonPath("$.firstName", equalTo("Rajendra")))
				.andExpect(jsonPath("$.lastName", equalTo("Thorat")));

	}

	@Test
	@DisplayName("POST /student Invalid Roll Number")
	void testAddNewStudentValidationError() throws Exception {

		Student studentRajendra = testDateHelper.getStudentRajendra();
		studentRajendra.setUserId(1L);
		studentRajendra.setRollNumber(99);
		doReturn(studentRajendra).when(studentService).addNewStudent(any());

		mockMvc.perform(post("/student").contentType(MediaType.APPLICATION_JSON).content(asJsonString(studentRajendra)))

				// Validate the response code and content type
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))

				// Validate the returned fields
				.andExpect(jsonPath("$.developerMessage",
						equalTo("org.springframework.web.bind.MethodArgumentNotValidException")))
				.andExpect(jsonPath("$.errors.rollNumber[0].code", equalTo("Min")));

	}

	@Test
	@DisplayName("PUT /student/{id} Found")
	void testUpdateStudent() throws Exception {

		Student studentRajendra = testDateHelper.getStudentRajendra();
		studentRajendra.setUserId(1L);
		doReturn(Optional.of(studentRajendra)).when(studentService).updateStudent(any());

		mockMvc.perform(
				put("/student/{id}", 1).contentType(MediaType.APPLICATION_JSON).content(asJsonString(studentRajendra)))

				// Validate the response code and content type
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))

				// Validate the returned fields
				.andExpect(jsonPath("$.userId", equalTo(1))).andExpect(jsonPath("$.firstName", equalTo("Rajendra")))
				.andExpect(jsonPath("$.lastName", equalTo("Thorat")));

	}

	@Test
	@DisplayName("PUT /student/{id} Not Found")
	void testUpdateStudentNotFound() throws Exception {

		Student studentRajendra = testDateHelper.getStudentRajendra();
		studentRajendra.setUserId(1L);
		doReturn(Optional.empty()).when(studentService).updateStudent(any());

		mockMvc.perform(
				put("/student/{id}", 1).contentType(MediaType.APPLICATION_JSON).content(asJsonString(studentRajendra)))
				// Validate the response code and content type
				.andExpect(status().isNotFound());
	}

	@Test
	@DisplayName("DELETE /student/{id} Not Found")
	void testDeleteStudentFound() throws Exception {

		Student studentRajendra = testDateHelper.getStudentRajendra();
		studentRajendra.setUserId(1L);
		doReturn(true).when(studentService).deleteStudentById(any());

		mockMvc.perform(delete("/student/{id}", 1).contentType(MediaType.APPLICATION_JSON))
				// Validate the response code and content type
				.andExpect(status().isOk());
	}

	@Test
	@DisplayName("DELETE /student/{id} Not Found")
	void testDeleteStudentNotFound() throws Exception {

		Student studentRajendra = testDateHelper.getStudentRajendra();
		studentRajendra.setUserId(1L);
		doReturn(false).when(studentService).deleteStudentById(any());

		mockMvc.perform(delete("/student/{id}", 1).contentType(MediaType.APPLICATION_JSON))
				// Validate the response code and content type
				.andExpect(status().isNotFound());
	}

}
