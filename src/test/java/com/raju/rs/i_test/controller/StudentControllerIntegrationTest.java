package com.raju.rs.i_test.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.raju.rs.entity.Student;
import com.raju.rs.repository.StudentRepository;
import com.raju.rs.util.TestDataHelper;

@DisplayName("Given Student, Branch and Institution")
@ActiveProfiles("test")
@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerIntegrationTest {

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private MockMvc mockMvc;

	private Student studentToTest;

	@Autowired
	private TestDataHelper testDateHelper;

	@BeforeAll
	public void setup() {
//		Institute instituteFC = testDateHelper.getInstituteFC();
//		instituteFC = instituteRepository.save(instituteFC);
//		
//		Branch branchFcCs = testDateHelper.getBranchFcCs();
//		branchFcCs.setInstitute(instituteFC);
//		branchFcCs = branchRepository.save(branchFcCs);

		Student studentRajendra = testDateHelper.getStudentRajendra();
		// MstudentRajendra.setBranch(branchFcCs);
		studentToTest = studentRepository.save(studentRajendra);

	}

	/**
	 * This is not required till In-Memory database is used, But if Oracle or MySQL
	 * is used then cleanup of test data is required
	 */
	@AfterAll
	public void cleanUp() {
//		studentRepository.delete(testDateHelper.getUserRajendra());
//		branchRepository.delete(testDateHelper.getBranchFcCs());
//		instituteRepository.delete(testDateHelper.getInstituteFC());
	}

	@Test
	@DisplayName("GET /student/1 - Found")
	void testGetStudentByIdFound() throws Exception {
		mockMvc.perform(get("/student/{id}", studentToTest.getUserId()))
				// Validate the response code and content type
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				// Validate the returned fields
				.andExpect(jsonPath("$.userId").value(studentToTest.getUserId()))
				.andExpect(jsonPath("$.firstName", equalTo("Rajendra")))
				.andExpect(jsonPath("$.lastName", equalTo("Thorat")));
	}

	@Test
	@DisplayName("GET /product/1 - Not Found")
	void testGetStudentByIdNotFound() throws Exception {

		// Execute the GET request
		mockMvc.perform(get("/product/{id}", 20123883))
				// Validate that we get a 404 Not Found response
				.andExpect(status().isNotFound());
	}

}
