package com.raju.rs.i_test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.raju.rs.entity.Student;
import com.raju.rs.repository.StudentRepository;
import com.raju.rs.service.StudentService;
import com.raju.rs.util.TestDataHelper;

@ExtendWith(SpringExtension.class)
public class StudentServiceUnitTest {

	@TestConfiguration
	static class StudentServiceTestConfiguration {

		@Bean
		public StudentService studentService() {
			return new StudentService();
		}

	}

	@Autowired
	private StudentService studentService;

	@MockBean
	private StudentRepository studentRepository;

	private TestDataHelper testDateHelper;

	@BeforeEach
	private void setup() {
		testDateHelper = new TestDataHelper();
	}

	@Test
	public void findByEmailIdTest() {

		Optional<Student> userRajendra = Optional.of(testDateHelper.getStudentRajendra());

		when(studentRepository.findByEmailId(userRajendra.get().getEmailId())).thenReturn(userRajendra);
		Optional<Student> found = studentService.findByEmailId(userRajendra.get().getEmailId());
		assertEquals(found.get().getEmailId(), userRajendra.get().getEmailId());
	}

	@Test
	public void findAllTest() {

		List<Student> allStudents = testDateHelper.getAllStudents();

		when(studentRepository.findAll()).thenReturn(allStudents);
		assertEquals(studentService.findAll().size(), 5);
	}

	@Test
	public void findAllEmptyTest() {

		when(studentRepository.findAll()).thenReturn(new ArrayList<>(0));
		assertEquals(studentService.findAll().size(), 0);
	}

	@Test
	public void findByIdTest() {

		Student rajendra = testDateHelper.getStudentRajendra();

		when(studentRepository.findById(1L)).thenReturn(Optional.of(rajendra));
		assertEquals(studentService.findById(1L).get().getFirstName(), "Rajendra");
	}

	@Test
	public void findByEmailIdAndPasswordFound() {

		Student rajendra = testDateHelper.getStudentRajendra();

		when(studentRepository.findByEmailIdAndPassword("rajendrat@xpanxion.co.in", "1234"))
				.thenReturn(Optional.of(rajendra));
		assertEquals(studentService.findByEmailIdAndPassword("rajendrat@xpanxion.co.in", "1234").get().getFirstName(),
				"Rajendra");
	}

	@Test
	public void findByEmailIdAndPasswordNotFound() {

		when(studentRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.empty());
		assertEquals(studentService.findByEmailIdAndPassword("rajendrat11@xpanxion.co.in", "xx1234"), Optional.empty());
	}

	@Test
	public void findByEmailIdFound() {

		Student rajendra = testDateHelper.getStudentRajendra();

		when(studentRepository.findByEmailId("rajendrat@xpanxion.co.in")).thenReturn(Optional.of(rajendra));
		assertEquals(studentService.findByEmailId("rajendrat@xpanxion.co.in").get().getFirstName(), "Rajendra");
	}

	@Test
	public void findByEmailIdNotFound() {

		when(studentRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.empty());
		assertEquals(studentService.findByEmailId("rajendrat11@xpanxion.co.in"), Optional.empty());
	}

	@Test
	public void addNewStudent() {

		Student rajendra = testDateHelper.getStudentRajendra();
		Student savedRajendra = testDateHelper.getStudentRajendra(true);
		savedRajendra.setUserId(1L);
		when(studentRepository.save(rajendra)).thenReturn(savedRajendra);
		assertEquals(studentService.addNewStudent(rajendra).getUserId(), new Long(1L));
	}

	@Test
	public void updateStudentFound() {

		Student toBeUpdated = testDateHelper.getStudentRajendra();
		toBeUpdated.setLastName("Thorat1");
		toBeUpdated.setUserId(1L);
		Student fromDB = testDateHelper.getStudentRajendra(true);
		when(studentRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(fromDB));
		assertEquals(studentService.updateStudent(toBeUpdated).get().getLastName(), "Thorat1");
	}

	@Test
	public void updateStudentNotFound() {

		Student rajendra = testDateHelper.getStudentRajendra();
		when(studentRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertEquals(studentService.updateStudent(rajendra), Optional.empty());
	}

	@Test
	public void deleteStudentFound() {

		Student fromDB = testDateHelper.getStudentRajendra();
		fromDB.setUserId(1L);
		when(studentRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(fromDB));
		assertEquals(studentService.deleteStudentById(1L), true);

	}

	@Test
	public void deleteStudentNotFound() {

		when(studentRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertEquals(studentService.deleteStudentById(1L), false);
	}
}
