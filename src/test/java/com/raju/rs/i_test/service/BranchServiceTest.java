package com.raju.rs.i_test.service;

import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.raju.rs.entity.Branch;
import com.raju.rs.repository.BranchRepository;
import com.raju.rs.service.BranchService;

@DisplayName("Given List of branches")
@ExtendWith(MockitoExtension.class)
public class BranchServiceTest {

	@InjectMocks
	private BranchService branchService;

	@Mock
	private BranchRepository branchRepository;

	@DisplayName("Find all should work")
	@Test
	final void testFindAllBranchs() {
		List<Branch> list = new ArrayList<>();
		list.add(new Branch());
		when(branchRepository.findAll()).thenReturn(list);
		// doReturn(list).when(branchRepository.findAll());
		assertEquals(1, branchService.findAllBranchs().size());
	}

}
