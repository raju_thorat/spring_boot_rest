package com.raju.rs.i_test.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.raju.rs.util.Sample;

@ExtendWith(MockitoExtension.class)
class SampleTest {

	@DisplayName("Given two integers for sum and sub")
	@Nested
	class SubSum {
		private Sample sample;

		@BeforeEach
		final void setUp() {
			sample = new Sample(10, 20);
		}

		@Test
		final void testSum() {
			assertEquals(30, sample.sum());

			assertAll("Verify multiple sum ops", () -> assertEquals(30, sample.sum()),
					() -> assertNotEquals(31, sample.sum()));
		}

		@Test
		final void testSub() {
			assertEquals(-10, sample.sub());
		}

		@AfterEach
		final void Destroy() {
			try {
				sample.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@DisplayName("Given two integers for multiply and divide")
	@Nested
	class DivMult {
		private Sample sample;

		@BeforeEach
		final void setUp() {
			sample = new Sample(10, 20);
		}

		@Test
		final void testSum() {
			assertEquals(200, sample.multiply());
		}

		@Test
		final void testSub() {
			assertEquals(0.0, sample.divide());
		}

		@AfterEach
		final void Destroy() {
			try {
				sample.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
