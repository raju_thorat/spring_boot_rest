package com.raju.rs.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.entity.Branch;
import com.raju.rs.entity.BranchYear;
import com.raju.rs.entity.Exam;
import com.raju.rs.entity.Institute;
import com.raju.rs.entity.Mark;
import com.raju.rs.entity.Role;
import com.raju.rs.entity.Student;
import com.raju.rs.entity.StudentBranchYearMapping;
import com.raju.rs.entity.Subject;
import com.raju.rs.entity.SubjectBranchYearMapping;
import com.raju.rs.entity.Teacher;
import com.raju.rs.repository.BranchRepository;
import com.raju.rs.repository.BranchYearRepository;
import com.raju.rs.repository.ExamRepository;
import com.raju.rs.repository.InstituteRepository;
import com.raju.rs.repository.MarkRepository;
import com.raju.rs.repository.RoleRepository;
import com.raju.rs.repository.StudentBranchYearMappingRepository;
import com.raju.rs.repository.StudentRepository;
import com.raju.rs.repository.SubjectBranchYearMappingRepository;
import com.raju.rs.repository.SubjectRepository;
import com.raju.rs.repository.TeacherRepository;
import com.raju.rs.util.TestDataHelper;

@Service("commonService")
public class CommonService {

	private static final Logger logger = LoggerFactory.getLogger(CommonService.class);

	private final StudentRepository studentRepository;
	private final TeacherRepository teacherRepository;
	private final InstituteRepository instituteRepository;
	private final BranchRepository branchRepository;
	private final SubjectRepository subjectRepository;
	private final MarkRepository marksRepository;
	private final BranchYearRepository branchYearRepository;
	private final ExamRepository examRepository;
	private final RoleRepository roleRepository;

	@Autowired
	private TestDataHelper testData;

	@Autowired
	private StudentBranchYearMappingRepository studentBranchYearMappingRepository;

	@Autowired
	private SubjectBranchYearMappingRepository subjectBranchYearMappingRepository;

	@Autowired
	public CommonService(StudentRepository studentRepository, TeacherRepository teacherRepository,
			InstituteRepository instituteRepository, BranchRepository branchRepository,
			SubjectRepository subjectRepository, MarkRepository marksRepository,
			BranchYearRepository branchYearRepository, ExamRepository examRepository, RoleRepository roleRepository) {
		this.studentRepository = studentRepository;
		this.teacherRepository = teacherRepository;
		this.instituteRepository = instituteRepository;
		this.branchRepository = branchRepository;
		this.subjectRepository = subjectRepository;
		this.marksRepository = marksRepository;
		this.branchYearRepository = branchYearRepository;
		this.examRepository = examRepository;
		this.roleRepository = roleRepository;
	}

	@Transactional
	public void addInitialTestData() {

		logger.debug("Adding test data.");
		Institute instituteFergusson = testData.getInstituteFC();

		Role roleStudent = roleRepository.save(testData.getRoleStudent());

		Role roleAdmin = roleRepository.save(testData.getRoleAdmin());

		Branch branchFcCs = testData.getBranchFcCs();

		Teacher teacherJohn = testData.getTeacherJohn();

		Student userRajendra = testData.getStudentRajendra();

		Student userVivek = testData.getStudentVivek();

		Student userPrashant = testData.getStudentPrashant();

		Student userSumeet = testData.getStudentSumeet();

		Student userPriti = testData.getStudentPriti();

		Subject subjectMath = testData.getSubjectMath();

		Subject subjectPhysics = testData.getSubjectPhysics();

		Subject subjectChemistry = testData.getSubjectChemistry();

		Subject subjectBiology = testData.getSubjectBiology();

		instituteFergusson = instituteRepository.save(instituteFergusson);

		branchFcCs = branchRepository.save(branchFcCs);

		teacherJohn.setBranch(branchFcCs);
		teacherJohn.getRoles().add(roleAdmin);
		teacherJohn = teacherRepository.save(teacherJohn);

		userRajendra.getRoles().add(roleStudent);
		userRajendra = studentRepository.save(userRajendra);

		userVivek.getRoles().add(roleStudent);
		userVivek = studentRepository.save(userVivek);

		userPrashant.getRoles().add(roleStudent);
		userPrashant = studentRepository.save(userPrashant);

		userSumeet.getRoles().add(roleStudent);
		userSumeet = studentRepository.save(userSumeet);

		userPriti.getRoles().add(roleStudent);
		userPriti = studentRepository.save(userPriti);

		subjectMath = subjectRepository.save(subjectMath);
		subjectPhysics = subjectRepository.save(subjectPhysics);
		subjectChemistry = subjectRepository.save(subjectChemistry);
		subjectBiology = subjectRepository.save(subjectBiology);

		BranchYear branchYear1stCS = testData.getBranchYear1stCS();
		branchYear1stCS = branchYearRepository.save(branchYear1stCS);

		SubjectBranchYearMapping biologyBranchYearMapping = testData.getBiologyBranchMapping();
		biologyBranchYearMapping.setBranchYear(branchYear1stCS);
		biologyBranchYearMapping.setSubject(subjectBiology);
		subjectBranchYearMappingRepository.save(biologyBranchYearMapping);

		SubjectBranchYearMapping chemistryBranchYearMapping = testData.getBiologyBranchMapping();
		chemistryBranchYearMapping.setBranchYear(branchYear1stCS);
		chemistryBranchYearMapping.setSubject(subjectChemistry);
		chemistryBranchYearMapping = subjectBranchYearMappingRepository.save(chemistryBranchYearMapping);

		SubjectBranchYearMapping physicsBranchYearMapping = testData.getBiologyBranchMapping();
		physicsBranchYearMapping.setBranchYear(branchYear1stCS);
		physicsBranchYearMapping.setSubject(subjectPhysics);
		physicsBranchYearMapping = subjectBranchYearMappingRepository.save(physicsBranchYearMapping);

		SubjectBranchYearMapping mathBranchYearMapping = testData.getBiologyBranchMapping();
		mathBranchYearMapping.setBranchYear(branchYear1stCS);
		mathBranchYearMapping.setSubject(subjectMath);
		mathBranchYearMapping = subjectBranchYearMappingRepository.save(mathBranchYearMapping);

		StudentBranchYearMapping rajendraBranchYearMapping = testData.getRajendraBranchYearMapping();
		rajendraBranchYearMapping.setBranchYear(branchYear1stCS);
		rajendraBranchYearMapping.setStudent(userRajendra);
		rajendraBranchYearMapping = studentBranchYearMappingRepository.save(rajendraBranchYearMapping);

		StudentBranchYearMapping vivekBranchYearMapping = testData.getVivekBranchYearMapping();
		vivekBranchYearMapping.setBranchYear(branchYear1stCS);
		vivekBranchYearMapping.setStudent(userVivek);
		vivekBranchYearMapping = studentBranchYearMappingRepository.save(vivekBranchYearMapping);

		StudentBranchYearMapping prashantBranchYearMapping = testData.getPrashantBranchYearMapping();
		prashantBranchYearMapping.setBranchYear(branchYear1stCS);
		prashantBranchYearMapping.setStudent(userPrashant);
		prashantBranchYearMapping = studentBranchYearMappingRepository.save(prashantBranchYearMapping);

		StudentBranchYearMapping sumeetBranchYearMapping = testData.getSumeetBranchYearMapping();
		sumeetBranchYearMapping.setBranchYear(branchYear1stCS);
		sumeetBranchYearMapping.setStudent(userSumeet);
		sumeetBranchYearMapping = studentBranchYearMappingRepository.save(sumeetBranchYearMapping);

		StudentBranchYearMapping pritiBranchYearMapping = testData.getPritiBranchYearMapping();
		pritiBranchYearMapping.setBranchYear(branchYear1stCS);
		pritiBranchYearMapping.setStudent(userPriti);
		pritiBranchYearMapping = studentBranchYearMappingRepository.save(pritiBranchYearMapping);

		Exam examEntity = testData.getExamEntity();
		examRepository.save(examEntity);

		List<Mark> rajendraMarks = testData.getRajendraMarks();
		marksRepository.saveAll(rajendraMarks);

		List<Mark> vivekMarks = testData.getVivekMarks();
		marksRepository.saveAll(vivekMarks);

		List<Mark> prashantMarks = testData.getPrashantMarks();
		marksRepository.saveAll(prashantMarks);

		List<Mark> poonamMarks = testData.getSumeetMarks();
		marksRepository.saveAll(poonamMarks);

		List<Mark> pritiMarks = testData.getPritiMarks();
		marksRepository.saveAll(pritiMarks);

	}
}
