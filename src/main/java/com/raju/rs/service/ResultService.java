package com.raju.rs.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.common.constants.enums.BranchYearTypeEnum;
import com.raju.rs.common.constants.enums.ExamTypeEnum;
import com.raju.rs.entity.Mark;
import com.raju.rs.repository.MarkRepository;

@Service("resultService")
public class ResultService {

	private static final Logger logger = LoggerFactory.getLogger(ResultService.class);

	private final MarkRepository marksRepository;

	@Autowired
	public ResultService(MarkRepository marksRepository) {
		this.marksRepository = marksRepository;
	}

	@Transactional(readOnly = true)
	public List<Mark> getResultForStudent(Long userId, Integer year, String branchYearType, String examType) {
		logger.debug("Getting result for student {0} for exam {1},{2},{3}", userId, branchYearType, year, examType);
		List<Mark> marksList = marksRepository.getResultForStudent(userId, year,
				BranchYearTypeEnum.valueOf(branchYearType), ExamTypeEnum.valueOf(examType));
		return marksList;
	}

}
