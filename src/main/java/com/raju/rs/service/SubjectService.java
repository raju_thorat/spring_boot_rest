package com.raju.rs.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.entity.Subject;
import com.raju.rs.repository.SubjectRepository;

@Service("subjectService")
public class SubjectService {

	private static final Logger logger = LoggerFactory.getLogger(SubjectService.class);

	private final SubjectRepository subjectRepository;

	@Autowired
	public SubjectService(SubjectRepository subjectRepository) {
		this.subjectRepository = subjectRepository;
	}

	@Transactional(readOnly = true)
	public List<Subject> findAllSubjects() {
		logger.debug("Fetching all subjects ...");
		// subjectRepository.inOnlyTest("");
		return subjectRepository.findAll();
	}
}
