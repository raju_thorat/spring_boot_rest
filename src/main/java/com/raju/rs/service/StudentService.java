package com.raju.rs.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.dto.UserDto;
import com.raju.rs.entity.Student;
import com.raju.rs.repository.StudentRepository;

@Service("studentService")
public class StudentService {

	private static final Logger logger = LoggerFactory.getLogger(StudentService.class);

	@Autowired
	private StudentRepository studentRepository;

	@Transactional(readOnly = true)
	public List<Student> findAll() {
		return studentRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Student> findById(Long id) {
		return studentRepository.findById(id);
	}

	@Transactional(readOnly = true)
	public Optional<Student> findByEmailIdAndPassword(String emailId, String password) {
		return studentRepository.findByEmailIdAndPassword(emailId, password);
	}

	@Transactional(readOnly = true)
	public Optional<Student> findByEmailId(String emailId) {
		return studentRepository.findByEmailId(emailId);
	}

	@Transactional
	public Student addNewStudent(Student student) {
		logger.info("Adding new student : {}", student);
		return studentRepository.save(student);
	}

	@Transactional
	public Optional<Student> updateStudent(Student student) {
		Optional<Student> studentOpt = studentRepository.findById(student.getUserId());
		if (studentOpt.isPresent()) {
			Student currentStudent = studentOpt.get();
			currentStudent.setFirstName(student.getFirstName());
			currentStudent.setLastName(student.getLastName());
			currentStudent.setMiddleName(student.getMiddleName());
			currentStudent.setPassword(student.getPassword());
			return Optional.of(currentStudent);
		} else {
			return Optional.empty();
		}

	}

	@Transactional
	public Boolean deleteStudentById(Long id) {
		Optional<Student> studentOpt = studentRepository.findById(id);
		if (studentOpt.isPresent()) {
			Student student = studentOpt.get();
			studentRepository.delete(student);
			return true;
		} else {
			return false;
		}
	}

	public Optional<Student> login(UserDto user) {
		return studentRepository.findByEmailIdAndPassword(user.getUsername(), user.getPassword());
	}

}
