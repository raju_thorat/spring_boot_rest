package com.raju.rs.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.dto.UserDto;
import com.raju.rs.entity.Teacher;
import com.raju.rs.repository.TeacherRepository;

@Service("teacherService")
public class TeacherService {

	private static final Logger logger = LoggerFactory.getLogger(TeacherService.class);

	@Autowired
	private TeacherRepository teacherRepository;

	@Transactional(readOnly = true)
	public List<Teacher> findAll() {
		return teacherRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Teacher> findById(Long id) {
		return teacherRepository.findById(id);
	}

	@Transactional(readOnly = true)
	public Optional<Teacher> findByEmailIdAndPassword(String emailId, String password) {
		return teacherRepository.findByEmailIdAndPassword(emailId, password);
	}

	@Transactional(readOnly = true)
	public Optional<Teacher> findByEmailId(String emailId) {
		return teacherRepository.findByEmailId(emailId);
	}

	@Transactional
	public Teacher addNewTeacher(Teacher teacher) {
		logger.info("Adding new teacher : {}", teacher);
		return teacherRepository.save(teacher);
	}

	@Transactional
	public Optional<Teacher> updateTeacher(Teacher teacher) {
		Optional<Teacher> teacherOpt = teacherRepository.findById(teacher.getUserId());
		if (teacherOpt.isPresent()) {
			Teacher currentTeacher = teacherOpt.get();
			currentTeacher.setFirstName(teacher.getFirstName());
			currentTeacher.setLastName(teacher.getLastName());
			currentTeacher.setMiddleName(teacher.getMiddleName());
			currentTeacher.setPassword(teacher.getPassword());
			return Optional.of(currentTeacher);
		} else {
			return Optional.empty();
		}

	}

	@Transactional
	public Boolean deleteTeacherById(Long id) {
		Optional<Teacher> teacherOpt = teacherRepository.findById(id);
		if (teacherOpt.isPresent()) {
			Teacher teacher = teacherOpt.get();
			teacherRepository.delete(teacher);
			return true;
		} else {
			return false;
		}
	}

	public Optional<Teacher> login(UserDto user) {
		return teacherRepository.findByEmailIdAndPassword(user.getUsername(), user.getPassword());
	}

}
