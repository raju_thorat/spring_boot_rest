package com.raju.rs.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.raju.rs.entity.Institute;
import com.raju.rs.repository.InstituteRepository;

@Service("instituteService")
public class InstituteService {

	private static final Logger logger = LoggerFactory.getLogger(InstituteService.class);

	private final InstituteRepository instituteRepository;

	@Autowired
	public InstituteService(InstituteRepository instituteRepository) {
		this.instituteRepository = instituteRepository;
	}

	@Transactional(readOnly = true)
	public List<Institute> findAllInstitutes() {
		logger.debug("Fetching all institutes ...");
		// instituteRepository.inOnlyTest("");
		return instituteRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Optional<Institute> findByInstituteId(Long id) {
		return instituteRepository.findByInstituteId(id);
	}

	@Transactional
	public Institute addNewInstitute(Institute institute) {
		logger.info("Adding new institute : {}", institute);
		return instituteRepository.save(institute);
	}

	@Transactional
	public Optional<Institute> updateInstitute(Institute institute) {
		Optional<Institute> instituteOpt = instituteRepository.findByInstituteId(institute.getInstituteId());
		if (instituteOpt.isPresent()) {
			Institute instituteFromDB = instituteOpt.get();
			instituteFromDB.setName(institute.getName());
			return Optional.of(instituteFromDB);
		} else {
			return Optional.empty();
		}

	}

	@Transactional
	public Boolean deleteInstituteById(Long id) {
		Optional<Institute> instituteOpt = instituteRepository.findByInstituteId(id);
		if (instituteOpt.isPresent()) {
			Institute instituteFromDB = instituteOpt.get();
			instituteRepository.delete(instituteFromDB);
			return true;
		} else {
			return false;
		}
	}

}
