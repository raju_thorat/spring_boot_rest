package com.raju.rs.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.raju.rs.entity.Student;
import com.raju.rs.service.StudentService;

@RestController
@CrossOrigin
public class StudentController {

	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

	private final StudentService studentService;

	@Autowired
	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}

	@GetMapping("/student")
	public ResponseEntity<?> getAllStudents() {
		List<Student> studentList = studentService.findAll();
		if (!studentList.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(studentList);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@GetMapping("/student/{id}")
	public ResponseEntity<?> getStudentById(@PathVariable Long id) {
		return studentService.findById(id).map(student -> {
			return ResponseEntity.status(HttpStatus.OK).body(student);
		}).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
	}

	@PostMapping("/student")
	public ResponseEntity<Student> addNewStudent(@Valid @RequestBody Student student) {
		logger.info("Creating new student :", student);
		return ResponseEntity.status(HttpStatus.CREATED).body(studentService.addNewStudent(student));
	}

	@PutMapping("/student/{id}")
	public ResponseEntity<?> updateStudent(@Valid @RequestBody Student student, @PathVariable Long id) {
		logger.info("Updating student :" + student);

		Optional<Student> studentOpt = studentService.updateStudent(student);
		if (studentOpt.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(student);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@DeleteMapping("/student/{id}")
	public ResponseEntity<?> deleteStudent(@PathVariable Long id) {

		logger.info("Deleting student with ID {}", id);

		if (studentService.deleteStudentById(id)) {
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
}