//package com.raju.rs.controller;
//
//import java.util.Optional;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.raju.rs.dto.UserDto;
//import com.raju.rs.entity.RUser;
//import com.raju.rs.entity.Student;
//import com.raju.rs.entity.Teacher;
//import com.raju.rs.service.StudentService;
//import com.raju.rs.service.TeacherService;
//
//@RestController
//
//public class LoginController {
//
//	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
//
//	@Autowired
//	private StudentService studentService;
//
//	@Autowired
//	private TeacherService teacherService;
//
//	// TODO : Make separate Login for Student and Teacher/Administrator
//	@PostMapping("/login")
//	public ResponseEntity<UserDto> login(@RequestBody UserDto userDto) {
//		logger.info("Login user request:", userDto);
//		Optional<Student> studentOpt = studentService.login(userDto);
//		if (studentOpt.isPresent()) {
//			Student student = studentOpt.get();
//			setUserProps(student, userDto);
//			return ResponseEntity.status(HttpStatus.OK).body(userDto);
//		} else {
//			Optional<Teacher> teacherOpt = teacherService.login(userDto);
//			if (teacherOpt.isPresent()) {
//				Teacher teacher = teacherOpt.get();
//				setUserProps(teacher, userDto);
//				return ResponseEntity.status(HttpStatus.OK).body(userDto);
//			}
//			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
//		}
//	}
//
//	private void setUserProps(RUser user, UserDto dto) {
//		dto.setUserId(user.getUserId());
//		dto.setUserType(user.getUserType());
//		dto.setFirstName(user.getFirstName());
//		dto.setLastName(user.getLastName());
//		dto.setEmailId(user.getEmailId());
//	}
//}
