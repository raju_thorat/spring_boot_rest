package com.raju.rs.controller;

import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.raju.rs.service.CommonService;
import com.raju.rs.util.TimerUtil;

@Controller
@CrossOrigin
@RequestMapping("/")
public class PageController {

	private static final Logger logger = LoggerFactory.getLogger(PageController.class);

	private final CommonService commonService;

	@Autowired
	public PageController(CommonService commonService) {
		this.commonService = commonService;
	}

	@Autowired
	private Environment environment;

	@PostConstruct
	public void setup() {

		boolean anyMatch = Arrays.stream(this.environment.getActiveProfiles())
				.anyMatch(profile -> "test".equals(profile));
		if (!anyMatch) {
			TimerUtil timerUtil = new TimerUtil();
			logger.warn("Test data insertion about to start (If not Present) ...");
			commonService.addInitialTestData();
			timerUtil.stop("Test Data Insertion/Check");
		}

	}

	// @PreAuthorize("hasAuthority('ROLE_USER')")
	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String getIndexPage() {
		return "index";
	}

}