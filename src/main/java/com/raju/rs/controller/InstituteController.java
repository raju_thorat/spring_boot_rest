package com.raju.rs.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.raju.rs.entity.Institute;
import com.raju.rs.service.InstituteService;

@RestController
public class InstituteController {

	private static final Logger logger = LoggerFactory.getLogger(InstituteController.class);

	private InstituteService instituteService;

	public InstituteController(InstituteService instituteService) {
		this.instituteService = instituteService;
	}

	@GetMapping("/institute")
	public ResponseEntity<?> getAllInstitutes() {
		List<Institute> institutes = instituteService.findAllInstitutes();
		if (!institutes.isEmpty()) {
			return ResponseEntity.status(HttpStatus.OK).body(institutes);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@GetMapping("/institute/{id}")
	public ResponseEntity<?> getInstituteById(@PathVariable Long id) {
		return instituteService.findByInstituteId(id).map(institute -> {
			return ResponseEntity.status(HttpStatus.OK).body(institute);
		}).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
	}

	@PostMapping("/institute")
	public ResponseEntity<Institute> addNewInstitute(@Valid @RequestBody Institute institute) {
		logger.info("Creating new institute :", institute);
		return ResponseEntity.status(HttpStatus.CREATED).body(instituteService.addNewInstitute(institute));
	}

	@PutMapping("/institute/{id}")
	public ResponseEntity<?> updateInstitute(@Valid @RequestBody Institute institute, @PathVariable Long id) {
		logger.info("Updating institute :" + institute);

		Optional<Institute> instituteOpt = instituteService.updateInstitute(institute);
		if (instituteOpt.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(institute);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@DeleteMapping("/institute/{id}")
	public ResponseEntity<?> deleteInstitute(@PathVariable Long id) {

		logger.info("Deleting institute with ID {}", id);

		if (instituteService.deleteInstituteById(id)) {
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
}
