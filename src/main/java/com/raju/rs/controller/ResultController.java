package com.raju.rs.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.raju.rs.entity.Mark;
import com.raju.rs.exeptionhandling.NoDataFoundException;
import com.raju.rs.service.ResultService;

@RestController
@CrossOrigin
// @RequestMapping(value = "/users")
public class ResultController {

	private static final Logger logger = LoggerFactory.getLogger(ResultController.class);

	private final ResultService resultService;

	@Autowired
	public ResultController(ResultService resultService) {
		this.resultService = resultService;
	}

	@RequestMapping(value = "/result/user/{userId}/year/{year}/branchYear/{branchYearType}/examType/{examType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getResult(@PathVariable("userId") Long userId, @PathVariable("year") Integer year,
			@PathVariable("branchYearType") String branchYearType, @PathVariable("examType") String examType) {

		List<Mark> marksList = resultService.getResultForStudent(userId, year, branchYearType, examType);
		if (marksList.isEmpty()) {
			logger.debug("No result found in the system...");
			throw new NoDataFoundException("No result found in the system.");
		}
		return ResponseEntity.ok().body(marksList);
	}

}