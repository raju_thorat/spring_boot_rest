package com.raju.rs.dto;

public final class UserSearchResultDTO {

	private Long id;

	private String city;

	public UserSearchResultDTO() {
	}

	@Override
	public String toString() {
		return "UserSearchResultDTO [id=" + id + ", city=" + city + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}