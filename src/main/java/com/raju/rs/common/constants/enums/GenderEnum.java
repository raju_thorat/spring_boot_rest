package com.raju.rs.common.constants.enums;

public enum GenderEnum {
	MALE, FEMALE, NOT_DISCLOSED;

}