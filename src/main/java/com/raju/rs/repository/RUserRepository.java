package com.raju.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raju.rs.entity.RUser;

@Repository
public interface RUserRepository extends JpaRepository<RUser, Long> {
	RUser findByUsername(String username);
}
