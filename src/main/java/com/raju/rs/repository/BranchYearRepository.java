package com.raju.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.BranchYear;

public interface BranchYearRepository extends JpaRepository<BranchYear, Long> {
}