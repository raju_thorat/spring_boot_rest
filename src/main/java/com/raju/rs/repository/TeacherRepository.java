package com.raju.rs.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
	
	Optional<Teacher> findByUserId(Long id);

	Optional<Teacher> findByEmailId(String emailId);

	Optional<Teacher> findByEmailIdAndPassword(String emailId, String password);
}