package com.raju.rs.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.Institute;

public interface InstituteRepository extends JpaRepository<Institute, Long> {

	Optional<Institute> findByName(String string);

	Optional<Institute> findByInstituteId(Long instituteId);

}