package com.raju.rs.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.raju.rs.common.constants.enums.BranchYearTypeEnum;
import com.raju.rs.common.constants.enums.ExamTypeEnum;
import com.raju.rs.entity.Mark;

public interface MarkRepository extends JpaRepository<Mark, Long> {

	@Query(value = "SELECT me FROM Mark me WHERE me.student.userId = :userId AND me.exam.examType=:examType"
			+ " AND me.exam.branchYear.year =:year AND me.exam.branchYear.branchYearType =:branchYearType")
	List<Mark> getResultForStudent(@Param("userId") Long userId, @Param("year") Integer year,
			@Param("branchYearType") BranchYearTypeEnum branchYearType, @Param("examType") ExamTypeEnum examType);

}
