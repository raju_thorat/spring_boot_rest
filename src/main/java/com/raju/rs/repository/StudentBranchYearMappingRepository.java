package com.raju.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.StudentBranchYearMapping;

public interface StudentBranchYearMappingRepository extends JpaRepository<StudentBranchYearMapping, Long> {

}