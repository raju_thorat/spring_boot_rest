package com.raju.rs.repository.custom;

import java.util.List;

import com.raju.rs.dto.UserSearchResultDTO;

//Following strict naming conventions for correct results
//TODO Verify naming convention is essential at this interface level
public interface CustomUserRepository {

	List<UserSearchResultDTO> findByCustomSearchTerm(String searchTerm);

	void inOnlyTest(String inParam1);
}