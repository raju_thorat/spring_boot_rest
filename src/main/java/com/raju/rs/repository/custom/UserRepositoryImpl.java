package com.raju.rs.repository.custom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.raju.rs.dto.UserSearchResultDTO;

//#$#$ Name must be UserRepositoryImpl if main repository is UserRepository
//Otherwise spring data will try generate query method for findByCustomSearchTerm
/**
 * You create any type of custom objects using this class. Just follow the naming convention
 * 
 * @author raju
 *
 */
@Repository
public class UserRepositoryImpl implements CustomUserRepository {

	private static final String SEARCH_UserEntity_ENTRIES = "SELECT u.id, u.city FROM user u WHERE u.email=:email";

	private final NamedParameterJdbcTemplate jdbcTemplate;

	@PersistenceContext
	private EntityManager em;

	@Override
	public void inOnlyTest(String inParam1) {
		this.em.createNativeQuery("BEGIN in_only_test(:inParam1); END;").setParameter("inParam1", inParam1).executeUpdate();
	}

	@Autowired
	public UserRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	// @Transactional(readOnly = true)
	@Override
	public List<UserSearchResultDTO> findByCustomSearchTerm(String searchTerm) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("email", searchTerm);

		List<UserSearchResultDTO> searchResults = jdbcTemplate.query(SEARCH_UserEntity_ENTRIES, queryParams,
				new BeanPropertyRowMapper<>(UserSearchResultDTO.class));

		return searchResults;
	}
}