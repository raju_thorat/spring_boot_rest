package com.raju.rs.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

	Optional<Student> findByUserId(Long id);

	Optional<Student> findByEmailId(String emailId);

	Optional<Student> findByEmailIdAndPassword(String emailId, String password);
}