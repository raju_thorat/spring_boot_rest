package com.raju.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raju.rs.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
