package com.raju.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.Exam;

public interface ExamRepository extends JpaRepository<Exam, Long> {
}