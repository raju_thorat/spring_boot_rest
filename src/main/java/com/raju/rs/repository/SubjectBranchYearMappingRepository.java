package com.raju.rs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.SubjectBranchYearMapping;

public interface SubjectBranchYearMappingRepository extends JpaRepository<SubjectBranchYearMapping, Long> {

}