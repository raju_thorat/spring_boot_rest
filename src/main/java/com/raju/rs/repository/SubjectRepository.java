package com.raju.rs.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.Student;
import com.raju.rs.entity.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Long> {

	Optional<Student> findBySubjectId(Long subjectId);

	Optional<Student> findByName(String name);

}