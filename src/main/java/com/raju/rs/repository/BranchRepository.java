package com.raju.rs.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raju.rs.entity.Branch;

public interface BranchRepository extends JpaRepository<Branch, Long> {

	Optional<Branch> findByName(String string);

	Optional<Branch> findByBranchId(Long instituteId);
}