package com.raju.rs.config.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.raju.rs.entity.RUser;
import com.raju.rs.repository.RUserRepository;

@Service
public class UserPrincipalDetailsService implements UserDetailsService {
	private RUserRepository userRepository;

	public UserPrincipalDetailsService(RUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		RUser user = this.userRepository.findByUsername(s);
		UserPrincipal userPrincipal = new UserPrincipal(user);

		return userPrincipal;
	}
}
