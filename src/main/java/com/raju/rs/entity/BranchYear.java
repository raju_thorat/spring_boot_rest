package com.raju.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.raju.rs.common.constants.enums.BranchYearTypeEnum;

/**
 * Since many activities can related to year and batch, so not adding this data
 * in exam table
 * 
 * @author rajendrat
 *
 */
@Entity
@Table(name = "BRANCH_YEAR")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "branchYearId")
public class BranchYear implements Serializable, Comparable<BranchYear> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "BRANCH_YEAR_ID")
	private Long branchYearId;

	@Basic(optional = false)
	@Column(name = "YEAR", nullable = false)
	private Integer year;

	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	@Column(name = "BRANCH_YEAR_TYPE", nullable = false)
	private BranchYearTypeEnum branchYearType;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
	private Branch branch;

	public BranchYear() {
		super();
	}

	private BranchYear(BranchYearBuilder builder) {
		this.branchYearId = builder.branchYearId;
		this.year = builder.year;
		this.branchYearType = builder.branchYearType;
		this.branch = builder.branch;
	}

	public static class BranchYearBuilder {
		private Long branchYearId;
		private Integer year;
		private BranchYearTypeEnum branchYearType;
		private Branch branch;

		public BranchYearBuilder(Long branchYearId) {
			this.branchYearId = branchYearId;
		}

		public BranchYear build() {
			return new BranchYear(this);
		}

		public BranchYearBuilder setYear(Integer year) {
			this.year = year;
			return this;
		}

		public BranchYearBuilder setBranchYearType(BranchYearTypeEnum branchYearType) {
			this.branchYearType = branchYearType;
			return this;
		}

		public BranchYearBuilder setBranchEntity(Branch branch) {
			this.branch = branch;
			return this;
		}

	}

	@Override
	public int compareTo(BranchYear o) {
		return this.branchYearId.compareTo(o.branchYearId);
	}

	public Long getBranchYearId() {
		return branchYearId;
	}

	public void setBranchYearId(Long id) {
		this.branchYearId = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public BranchYearTypeEnum getBranchYearType() {
		return branchYearType;
	}

	public void setBranchYearType(BranchYearTypeEnum branchYearType) {
		this.branchYearType = branchYearType;
	}

}
