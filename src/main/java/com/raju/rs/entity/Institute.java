package com.raju.rs.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "INSTITUTE")
public class Institute implements Serializable, Comparable<Institute> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "INSTITUTE_ID")
	private Long instituteId;

	@Basic(optional = false)
	@Column(name = "NAME", unique = true, nullable = false)
	private String name;

	@OrderBy("INSTITUTE_ID")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "institute")
	@JsonIgnore
	private Set<Branch> branches;

	public Institute() {
		super();
	}

	private Institute(InstituteBuilder builder) {
		this.instituteId = builder.id;
		this.name = builder.name;
		this.branches = builder.branches;
	}

	public static class InstituteBuilder {
		private Long id;
		private String name;
		private Set<Branch> branches;

		public InstituteBuilder(Long id) {
			this.id = id;
		}

		public InstituteBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public InstituteBuilder setBranches(Set<Branch> branches) {
			this.branches = branches;
			return this;
		}

		public Institute build() {
			return new Institute(this);
		}
	}

	@Override
	public int compareTo(Institute o) {
		return this.name.compareTo(o.name);
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long id) {
		this.instituteId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Branch> getBranches() {
		return branches;
	}

	public void setBranches(Set<Branch> branches) {
		this.branches = branches;
	}

}
