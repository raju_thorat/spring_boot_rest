package com.raju.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "STUDENT_BRANCH_YEAR", uniqueConstraints = @UniqueConstraint(columnNames = { "BRANCH_YEAR_ID", "STUDENT_ID" }))
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "studentBranchYearId")
public class StudentBranchYearMapping implements Serializable, Comparable<StudentBranchYearMapping> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "STUDENT_BRANCH_YEAR_ID")
	private Long studentBranchYearId;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "STUDENT_ID", nullable = false)
	private Student student;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_YEAR_ID", nullable = false)
	private BranchYear branchYear;

	public StudentBranchYearMapping() {
		super();
	}

	private StudentBranchYearMapping(StudentBranchYearMappingBuilder builder) {
		this.studentBranchYearId = builder.studentBranchYearId;
		this.student = builder.student;
		this.branchYear = builder.branchYear;
	}

	public static class StudentBranchYearMappingBuilder {
		private Long studentBranchYearId;
		private Student student;
		private BranchYear branchYear;

		public StudentBranchYearMappingBuilder(Long studentBranchYearId) {
			this.studentBranchYearId = studentBranchYearId;
		}

		public StudentBranchYearMappingBuilder setStudent(Student student) {
			this.student = student;
			return this;
		}

		public StudentBranchYearMappingBuilder setBranchYear(BranchYear branchYear) {
			this.branchYear = branchYear;
			return this;
		}

		public StudentBranchYearMapping build() {
			return new StudentBranchYearMapping(this);
		}

	}

	@Override
	public int compareTo(StudentBranchYearMapping o) {
		return this.studentBranchYearId.compareTo(o.studentBranchYearId);
	}

	public Long getStudentBranchYearId() {
		return studentBranchYearId;
	}

	public void setStudentBranchYearId(Long studentBranchYearId) {
		this.studentBranchYearId = studentBranchYearId;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public BranchYear getBranchYear() {
		return branchYear;
	}

	public void setBranchYear(BranchYear branchYear) {
		this.branchYear = branchYear;
	}

}
