package com.raju.rs.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "MARK")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "markId")
public class Mark {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "MARK_ID")
	private Long markId;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "EXAM_ID", referencedColumnName = "EXAM_ID", nullable = false)
	private Exam exam;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "SUBJECT_ID", referencedColumnName = "SUBJECT_ID", nullable = false)
	private Subject subject;

	@Basic(optional = false)
	@Column(name = "MARKS")
	private Integer marks;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "STUDENT_ID", referencedColumnName = "USER_ID", nullable = false)
	private Student student;

	public Mark() {
		super();
	}

	private Mark(MarkBuilder builder) {
		super();
		this.markId = builder.id;
		this.exam = builder.exam;
		this.subject = builder.subject;
		this.marks = builder.marks;
		this.student = builder.student;
	}

	public static class MarkBuilder {
		private Long id;
		private Exam exam;
		private Subject subject;
		private Integer marks;
		private Student student;

		public MarkBuilder(Long id, Exam exam, Subject subject, Integer marks, Student student) {
			this.id = id;
			this.exam = exam;
			this.subject = subject;
			this.marks = marks;
			this.student = student;
		}

		public Mark build() {
			return new Mark(this);
		}

	}

	public Long getMarkId() {
		return markId;
	}

	public void setMarkId(Long id) {
		this.markId = id;
	}

	public Integer getMarks() {
		return marks;
	}

	public void setMarks(Integer marks) {
		this.marks = marks;
	}

	public Exam getExam() {
		return exam;
	}

	public void setExam(Exam exam) {
		this.exam = exam;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
}
