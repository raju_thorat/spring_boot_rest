package com.raju.rs.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.raju.rs.common.constants.enums.GenderEnum;

@Entity
@Table(name = "TEACHER")
@DiscriminatorValue(value = "TEACHER")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userId")
public class Teacher extends RUser implements Serializable, Comparable<Teacher> {

	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "TEACHER_NUMBER", nullable = false)
	private Integer teacherNumber;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_ID", referencedColumnName = "BRANCH_ID", nullable = false)
	protected Branch branch;

	public Teacher() {
		super();
	}

	@Override
	public int compareTo(Teacher o) {
		return this.teacherNumber.compareTo(o.teacherNumber);
	}

	private Teacher(TeacherBuilder builder) {
		this.userId = builder.userId;
		this.emailId = builder.emailId;
		this.username = builder.username;
		this.password = builder.password;
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.userType = builder.userType;
		this.branch = builder.branch;
		this.middleName = builder.middleName;
		this.gender = builder.gender;
		this.birthDate = builder.birthDate;
		this.subjects = builder.subjects;
		this.roles = builder.roles;
		this.teacherNumber = builder.teacherNumber;
	}

	public static class TeacherBuilder {

		public String username;
		private Integer teacherNumber;
		private Long userId;
		private String emailId;
		private String password;
		private String firstName;
		private String lastName;
		private String userType;
		private Branch branch;

		private String middleName;
		private GenderEnum gender;
		private Date birthDate;
		private List<Subject> subjects;
		private Set<Role> roles;

		public TeacherBuilder(Long id) {
			super();
			this.userId = id;
		}

		public TeacherBuilder setTeacherNumber(Integer teacherNumber) {
			this.teacherNumber = teacherNumber;
			return this;
		}

		public TeacherBuilder setEmailId(String emailId) {
			this.emailId = emailId;
			return this;
		}

		public TeacherBuilder setPassword(String password) {
			this.password = password;
			return this;
		}

		public TeacherBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public TeacherBuilder setUsername(String username) {
			this.username = username;
			return this;
		}

		public TeacherBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public TeacherBuilder setUserType(String userType) {
			this.userType = userType;
			return this;
		}

		public TeacherBuilder setBranch(Branch branch) {
			this.branch = branch;
			return this;
		}

		public TeacherBuilder setMiddleName(String middleName) {
			this.middleName = middleName;
			return this;
		}

		public TeacherBuilder setGender(GenderEnum gender) {
			this.gender = gender;
			return this;
		}

		public TeacherBuilder setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
			return this;
		}

		public TeacherBuilder setSubjects(List<Subject> subjects) {
			this.subjects = subjects;
			return this;
		}

		public TeacherBuilder setRoles(Set<Role> roles) {
			this.roles = roles;
			return this;
		}

		public Teacher build() {
			return new Teacher(this);
		}

	}

	public Integer getTeacherNumber() {
		return teacherNumber;
	}

	public void setTeacherNumber(Integer teacherNumber) {
		this.teacherNumber = teacherNumber;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

}
