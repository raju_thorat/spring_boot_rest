package com.raju.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "SUBJECT_BRANCH_YEAR", uniqueConstraints = @UniqueConstraint(columnNames = { "BRANCH_YEAR_ID", "SUBJECT_ID" }))
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "subjectBranchYearId")
public class SubjectBranchYearMapping implements Serializable, Comparable<SubjectBranchYearMapping> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "SUBJECT_BRANCH_YEAR_ID")
	private Long subjectBranchYearId;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "SUBJECT_ID", nullable = false)
	private Subject subject;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_YEAR_ID", nullable = false)
	private BranchYear branchYear;

	public SubjectBranchYearMapping() {
		super();
	}

	private SubjectBranchYearMapping(SubjectBranchYearMappingBuilder builder) {
		this.subjectBranchYearId = builder.subjectBranchYearId;
		this.subject = builder.subject;
		this.branchYear = builder.branchYear;
	}

	public static class SubjectBranchYearMappingBuilder {
		private Long subjectBranchYearId;
		private Subject subject;
		private BranchYear branchYear;

		public SubjectBranchYearMappingBuilder(Long subjectBranchYearId) {
			this.subjectBranchYearId = subjectBranchYearId;
		}

		public SubjectBranchYearMappingBuilder setSubject(Subject subject) {
			this.subject = subject;
			return this;
		}

		public SubjectBranchYearMappingBuilder setBranchYear(BranchYear branchYear) {
			this.branchYear = branchYear;
			return this;
		}

		public SubjectBranchYearMapping build() {
			return new SubjectBranchYearMapping(this);
		}

	}

	@Override
	public int compareTo(SubjectBranchYearMapping o) {
		return this.subjectBranchYearId.compareTo(o.subjectBranchYearId);
	}

	public Long getSubjectBranchYearId() {
		return subjectBranchYearId;
	}

	public void setSubjectBranchYearId(Long subjectBranchYearId) {
		this.subjectBranchYearId = subjectBranchYearId;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public BranchYear getBranchYear() {
		return branchYear;
	}

	public void setBranchYear(BranchYear branchYear) {
		this.branchYear = branchYear;
	}

}
