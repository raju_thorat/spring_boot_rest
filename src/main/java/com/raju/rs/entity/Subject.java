package com.raju.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 *
 * @author raju
 */
// unique constraint must have database column names
@Entity
@Table(name = "SUBJECT", uniqueConstraints = @UniqueConstraint(columnNames = { "NAME" }))
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "subjectId")
public class Subject implements Serializable, Comparable<Subject> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "SUBJECT_ID")
	private Long subjectId;

	@Basic(optional = false)
	@Column(name = "NAME", nullable = false)
	private String name;

	public Subject() {
		super();
	}

	private Subject(SubjectBuilder builder) {
		this.subjectId = builder.id;
		this.name = builder.name;
	}

	public static class SubjectBuilder {

		private Long id;
		private String name;

		public SubjectBuilder(Long id) {
			this.id = id;
		}

		public SubjectBuilder setName(String name) {
			this.name = name;
			return this;
		}

		public Subject build() {
			return new Subject(this);
		}
	}

	@Override
	public int compareTo(Subject o) {
		return this.name.compareTo(o.getName());
	}

	public Subject(Long id) {
		this.subjectId = id;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long id) {
		this.subjectId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
