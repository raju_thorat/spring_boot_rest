package com.raju.rs.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.raju.rs.common.constants.enums.GenderEnum;

@Entity
@Table(name = "STUDENT")
@DiscriminatorValue(value = "STUDENT")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userId")
public class Student extends RUser implements Serializable, Comparable<Student> {

	private static final long serialVersionUID = 1L;

	@Basic(optional = false)
	@Column(name = "ROLL_NUMBER", nullable = false)
	@Min(100)
	@Max(999)
	private Integer rollNumber;

	public Student() {
		super();
	}

	@Override
	public int compareTo(Student o) {
		return this.rollNumber.compareTo(o.rollNumber);
	}

	private Student(StudentBuilder builder) {
		this.userId = builder.userId;
		this.emailId = builder.emailId;
		this.username = builder.username;
		this.password = builder.password;
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.userType = builder.userType;
		this.middleName = builder.middleName;
		this.gender = builder.gender;
		this.birthDate = builder.birthDate;
		this.subjects = builder.subjects;
		this.roles = builder.roles;
		this.rollNumber = builder.rollNumber;
	}

	public static class StudentBuilder {

		private Long userId;
		private Integer rollNumber;
		private String emailId;
		private String username;
		private String password;
		private String firstName;
		private String lastName;
		private String userType;
		private String middleName;
		private GenderEnum gender;
		private Date birthDate;
		private List<Subject> subjects;
		private Set<Role> roles;

		public StudentBuilder(Long id) {
			super();
			this.userId = id;
		}

		public StudentBuilder setRollNumber(Integer rollNumber) {
			this.rollNumber = rollNumber;
			return this;
		}

		public StudentBuilder setEmailId(String emailId) {
			this.emailId = emailId;
			return this;
		}

		public StudentBuilder setPassword(String password) {
			this.password = password;
			return this;
		}

		public StudentBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public StudentBuilder setUsername(String username) {
			this.username = username;
			return this;
		}

		public StudentBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public StudentBuilder setUserType(String userType) {
			this.userType = userType;
			return this;
		}

		public StudentBuilder setMiddleName(String middleName) {
			this.middleName = middleName;
			return this;
		}

		public StudentBuilder setGender(GenderEnum gender) {
			this.gender = gender;
			return this;
		}

		public StudentBuilder setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
			return this;
		}

		public StudentBuilder setSubjects(List<Subject> subjects) {
			this.subjects = subjects;
			return this;
		}

		public StudentBuilder setRoles(Set<Role> roles) {
			this.roles = roles;
			return this;
		}

		public Student build() {
			return new Student(this);
		}

	}

	public Integer getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(Integer rollNumber) {
		this.rollNumber = rollNumber;
	}
}
