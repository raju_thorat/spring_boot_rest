package com.raju.rs.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.raju.rs.common.constants.enums.ExamTypeEnum;

@Entity
@Table(name = "EXAM")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "examId")
public class Exam implements Serializable, Comparable<Exam> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Basic(optional = false)
	@Column(name = "EXAM_ID")
	private Long examId;

	@Basic(optional = false)
	@Enumerated(EnumType.STRING)
	@Column(name = "EXAM_TYPE", nullable = false)
	private ExamTypeEnum examType;

	@Basic(optional = false)
	@ManyToOne
	@JoinColumn(name = "BRANCH_YEAR_ID", referencedColumnName = "BRANCH_YEAR_ID", nullable = false)
	private BranchYear branchYear;

	public Exam() {
		super();
	}

	private Exam(ExamBuilder builder) {
		this.examId = builder.id;
		this.examType = builder.examType;
		this.branchYear = builder.branchYear;
	}

	public static class ExamBuilder {

		private Long id;
		private ExamTypeEnum examType;
		private BranchYear branchYear;

		public ExamBuilder(Long id) {
			this.id = id;
		}

		public ExamBuilder setExamType(ExamTypeEnum examType) {
			this.examType = examType;
			return this;
		}

		public ExamBuilder setBranchYear(BranchYear branchYear) {
			this.branchYear = branchYear;
			return this;
		}

		public Exam build() {
			return new Exam(this);
		}
	}

	@Override
	public int compareTo(Exam o) {
		return this.examId.compareTo(o.examId);
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long id) {
		this.examId = id;
	}

	public ExamTypeEnum getExamType() {
		return examType;
	}

	public void setExamType(ExamTypeEnum examType) {
		this.examType = examType;
	}

	public BranchYear getBranchYear() {
		return branchYear;
	}

	public void setBranchYear(BranchYear branchYear) {
		this.branchYear = branchYear;
	}

}
