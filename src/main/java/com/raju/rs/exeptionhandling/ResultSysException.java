package com.raju.rs.exeptionhandling;

public class ResultSysException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ResultSysException(String message) {
		super(message);
	}

}
