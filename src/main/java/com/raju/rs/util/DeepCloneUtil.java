package com.raju.rs.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Deep clone using in memory I/O
 * 
 * @author raju
 *
 */
public class DeepCloneUtil {
	private static final Logger logger = LoggerFactory.getLogger(DeepCloneUtil.class);

	public static final Object deepClone(Object objToClone) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(objToClone);
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			logger.error("Deep clonning failed !" + objToClone);
			return null;
		}
	}

}