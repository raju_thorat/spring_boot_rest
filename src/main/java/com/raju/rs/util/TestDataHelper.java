package com.raju.rs.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.raju.rs.common.constants.enums.BranchYearTypeEnum;
import com.raju.rs.common.constants.enums.ExamTypeEnum;
import com.raju.rs.common.constants.enums.GenderEnum;
import com.raju.rs.common.constants.enums.UserTypeEnum;
import com.raju.rs.entity.Branch;
import com.raju.rs.entity.BranchYear;
import com.raju.rs.entity.Exam;
import com.raju.rs.entity.Institute;
import com.raju.rs.entity.Mark;
import com.raju.rs.entity.Role;
import com.raju.rs.entity.Student;
import com.raju.rs.entity.StudentBranchYearMapping;
import com.raju.rs.entity.Subject;
import com.raju.rs.entity.SubjectBranchYearMapping;
import com.raju.rs.entity.Teacher;

@Component
@Scope("prototype") // Important for parallel test execution
public class TestDataHelper {

	private Institute instituteFC;
	private Branch branchFcCs;
	private Student studentRajendra, studentVivek, studentPrashant, studentSumeet, studentPriti;
	private Teacher teacherJohn;
	private Subject subjectMath, subjectPhysics, subjectChemistry, subjectBiology;
	private BranchYear branchYear1stCS;
	private Exam examEntity;
	private List<Mark> rajendraMarks, vivekMarks, prashantMarks, sumeetMarks, pritiMarks;
	private StudentBranchYearMapping rajendraBranchMapping, vivekBranchMapping, prashantBranchMapping,
			sumeetBranchMapping, pritiBranchMapping;
	private Role roleAdmin, roleStudent;
	private SubjectBranchYearMapping mathBranchMapping, physicsBranchMapping, chemistryBranchMapping,
			biologyBranchMapping;

	public Role getRoleAdmin(boolean... newValue) {
		if (roleAdmin == null || (newValue.length > 0 && newValue[0])) {
			roleAdmin = new Role(null, "ADMIN");
		}
		return roleAdmin;
	}

	public Role getRoleStudent(boolean... newValue) {
		if (roleStudent == null || (newValue.length > 0 && newValue[0])) {
			roleStudent = new Role(null, "STUDENT");
		}
		return roleStudent;
	}

	public Institute getInstituteFC(boolean... newValue) {
		if (instituteFC == null || (newValue.length > 0 && newValue[0])) {
			instituteFC = new Institute.InstituteBuilder(null).setName("Fergsson College").build();
		}
		return instituteFC;
	}

	public Branch getBranchFcCs(boolean... newValue) {
		if (branchFcCs == null || (newValue.length > 0 && newValue[0])) {
			branchFcCs = new Branch.BranchBuilder(null).setInstitute(instituteFC).setName("Computer Science").build();
		}
		return branchFcCs;
	}

	public Teacher getTeacherJohn(boolean... newValue) {
		if (teacherJohn == null || (newValue.length > 0 && newValue[0])) {
			teacherJohn = new Teacher.TeacherBuilder(null).setTeacherNumber(11).setEmailId("john@xpanxion.co.in")
					.setUsername("john").setPassword("1234").setFirstName("John").setLastName("Papa")
					.setUserType(UserTypeEnum.TEACHER.name()).setGender(GenderEnum.MALE).build();
		}
		return teacherJohn;

	}

	public Student getStudentRajendra(boolean... newValue) {
		if (studentRajendra == null || (newValue.length > 0 && newValue[0])) {
			studentRajendra = new Student.StudentBuilder(null).setRollNumber(100).setEmailId("rthorat@xpanxion.com")
					.setUsername("rthorat").setPassword("1234").setFirstName("Rajendra").setLastName("Thorat")
					.setUserType(UserTypeEnum.STUDENT.name()).setGender(GenderEnum.MALE).build();
		}
		return studentRajendra;

	}

	public Student getStudentVivek(boolean... newValue) {
		if (studentVivek == null || (newValue.length > 0 && newValue[0])) {
			studentVivek = new Student.StudentBuilder(null).setRollNumber(101).setEmailId("vivekk@xpanxion.co.in")
					.setUsername("vkumar").setPassword("101").setFirstName("Vivek").setLastName("Kumar")
					.setUserType(UserTypeEnum.STUDENT.name()).setGender(GenderEnum.MALE).build();
		}
		return studentVivek;
	}

	public Student getStudentPrashant(boolean... newValue) {
		if (studentPrashant == null || (newValue.length > 0 && newValue[0])) {
			studentPrashant = new Student.StudentBuilder(null).setRollNumber(102).setEmailId("pnalawade@xpanxion.co.in")
					.setPassword("1234").setFirstName("Prashant").setUsername("pnalawade").setLastName("Nalawade")
					.setUserType(UserTypeEnum.STUDENT.name()).setGender(GenderEnum.MALE).build();
		}
		return studentPrashant;
	}

	public Student getStudentSumeet(boolean... newValue) {
		if (studentSumeet == null || (newValue.length > 0 && newValue[0])) {
			studentSumeet = new Student.StudentBuilder(null).setRollNumber(103).setEmailId("sumeets@xpanxion.co.in")
					.setUsername("sumeets").setPassword("1234").setFirstName("Sumeet").setLastName("Sonamwane")
					.setUserType(UserTypeEnum.STUDENT.name()).setGender(GenderEnum.MALE).build();
		}
		return studentSumeet;
	}

	public Student getStudentPriti(boolean... newValue) {
		if (studentPriti == null || (newValue.length > 0 && newValue[0])) {
			studentPriti = new Student.StudentBuilder(null).setRollNumber(104).setEmailId("pritit@xpanxion.co.in")
					.setUsername("ptiwari").setPassword("1234").setFirstName("Priti").setLastName("Tiwari")
					.setUserType(UserTypeEnum.STUDENT.name()).setGender(GenderEnum.FEMALE).build();
		}
		return studentPriti;
	}

	public List<Student> getAllStudents(boolean... newValue) {
		return Arrays.asList(getStudentRajendra(true), getStudentPrashant(true), getStudentPriti(true),
				getStudentSumeet(true), getStudentVivek(true));
	}

	public Set<Subject> getAllSubjects(boolean... newValue) {
		return new LinkedHashSet<>(
				Arrays.asList(getSubjectBiology(), getSubjectChemistry(), getSubjectMath(), getSubjectPhysics()));
	}

	public Subject getSubjectMath(boolean... newValue) {
		if (subjectMath == null || (newValue.length > 0 && newValue[0])) {
			subjectMath = new Subject.SubjectBuilder(null).setName("Math").build();
		}
		return subjectMath;
	}

	public Subject getSubjectPhysics(boolean... newValue) {
		if (subjectPhysics == null || (newValue.length > 0 && newValue[0])) {
			subjectPhysics = new Subject.SubjectBuilder(null).setName("Physics").build();
		}
		return subjectPhysics;
	}

	public Subject getSubjectChemistry(boolean... newValue) {
		if (subjectChemistry == null || (newValue.length > 0 && newValue[0])) {
			subjectChemistry = new Subject.SubjectBuilder(null).setName("Chemistry").build();
		}
		return subjectChemistry;
	}

	public Subject getSubjectBiology(boolean... newValue) {
		if (subjectBiology == null || (newValue.length > 0 && newValue[0])) {
			subjectBiology = new Subject.SubjectBuilder(null).setName("Biology").build();
		}
		return subjectBiology;
	}

	public BranchYear getBranchYear1stCS(boolean... newValue) {
		if (branchYear1stCS == null || (newValue.length > 0 && newValue[0])) {
			branchYear1stCS = new BranchYear.BranchYearBuilder(null).setBranchEntity(branchFcCs)
					.setBranchYearType(BranchYearTypeEnum.FRIST_YEAR).setYear(2018).build();
		}
		return branchYear1stCS;
	}

	public Exam getExamEntity(boolean... newValue) {
		if (examEntity == null || (newValue.length > 0 && newValue[0])) {
			examEntity = new Exam.ExamBuilder(null).setBranchYear(branchYear1stCS).setExamType(ExamTypeEnum.SEMISTER)
					.build();
		}
		return examEntity;
	}

	public List<Mark> getRajendraMarks(boolean... newValue) {
		if (rajendraMarks == null || (newValue.length > 0 && newValue[0])) {
			rajendraMarks = new ArrayList<>(4);
			rajendraMarks.add(new Mark.MarkBuilder(null, examEntity, subjectMath, 80, studentRajendra).build());
			rajendraMarks.add(new Mark.MarkBuilder(null, examEntity, subjectPhysics, 70, studentRajendra).build());
			rajendraMarks.add(new Mark.MarkBuilder(null, examEntity, subjectChemistry, 52, studentRajendra).build());
			rajendraMarks.add(new Mark.MarkBuilder(null, examEntity, subjectBiology, 90, studentRajendra).build());
		}
		return rajendraMarks;
	}

	public List<Mark> getVivekMarks(boolean... newValue) {
		if (vivekMarks == null || (newValue.length > 0 && newValue[0])) {
			vivekMarks = new ArrayList<>(4);
			vivekMarks.add(new Mark.MarkBuilder(null, examEntity, subjectMath, 81, studentVivek).build());
			vivekMarks.add(new Mark.MarkBuilder(null, examEntity, subjectPhysics, 71, studentVivek).build());
			vivekMarks.add(new Mark.MarkBuilder(null, examEntity, subjectChemistry, 53, studentVivek).build());
			vivekMarks.add(new Mark.MarkBuilder(null, examEntity, subjectBiology, 91, studentVivek).build());
		}
		return vivekMarks;
	}

	public List<Mark> getPrashantMarks(boolean... newValue) {
		if (prashantMarks == null || (newValue.length > 0 && newValue[0])) {
			prashantMarks = new ArrayList<>(4);
			prashantMarks.add(new Mark.MarkBuilder(null, examEntity, subjectMath, 82, studentPrashant).build());
			prashantMarks.add(new Mark.MarkBuilder(null, examEntity, subjectPhysics, 72, studentPrashant).build());
			prashantMarks.add(new Mark.MarkBuilder(null, examEntity, subjectChemistry, 54, studentPrashant).build());
			prashantMarks.add(new Mark.MarkBuilder(null, examEntity, subjectBiology, 92, studentPrashant).build());
		}
		return prashantMarks;
	}

	public List<Mark> getSumeetMarks(boolean... newValue) {
		if (sumeetMarks == null || (newValue.length > 0 && newValue[0])) {
			sumeetMarks = new ArrayList<>(4);
			sumeetMarks.add(new Mark.MarkBuilder(null, examEntity, subjectMath, 83, studentSumeet).build());
			sumeetMarks.add(new Mark.MarkBuilder(null, examEntity, subjectPhysics, 73, studentSumeet).build());
			sumeetMarks.add(new Mark.MarkBuilder(null, examEntity, subjectChemistry, 55, studentSumeet).build());
			sumeetMarks.add(new Mark.MarkBuilder(null, examEntity, subjectBiology, 93, studentSumeet).build());
		}
		return sumeetMarks;
	}

	public List<Mark> getPritiMarks(boolean... newValue) {
		if (pritiMarks == null || (newValue.length > 0 && newValue[0])) {

			pritiMarks = new ArrayList<>(4);
			pritiMarks.add(new Mark.MarkBuilder(null, examEntity, subjectMath, 84, studentPriti).build());
			pritiMarks.add(new Mark.MarkBuilder(null, examEntity, subjectPhysics, 74, studentPriti).build());
			pritiMarks.add(new Mark.MarkBuilder(null, examEntity, subjectChemistry, 56, studentPriti).build());
			pritiMarks.add(new Mark.MarkBuilder(null, examEntity, subjectBiology, 94, studentPriti).build());
		}
		return pritiMarks;
	}

	public StudentBranchYearMapping getRajendraBranchYearMapping(boolean... newValue) {
		if (rajendraBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			rajendraBranchMapping = new StudentBranchYearMapping.StudentBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setStudent(studentRajendra).build();

		}
		return rajendraBranchMapping;
	}

	public StudentBranchYearMapping getVivekBranchYearMapping(boolean... newValue) {
		if (vivekBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			vivekBranchMapping = new StudentBranchYearMapping.StudentBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setStudent(studentVivek).build();
		}
		return vivekBranchMapping;
	}

	public StudentBranchYearMapping getPrashantBranchYearMapping(boolean... newValue) {
		if (prashantBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			prashantBranchMapping = new StudentBranchYearMapping.StudentBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setStudent(studentPrashant).build();
		}
		return prashantBranchMapping;
	}

	public StudentBranchYearMapping getSumeetBranchYearMapping(boolean... newValue) {
		if (sumeetBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			sumeetBranchMapping = new StudentBranchYearMapping.StudentBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setStudent(studentSumeet).build();
		}
		return sumeetBranchMapping;
	}

	public StudentBranchYearMapping getPritiBranchYearMapping(boolean... newValue) {
		if (pritiBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			pritiBranchMapping = new StudentBranchYearMapping.StudentBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setStudent(studentPriti).build();
		}
		return pritiBranchMapping;
	}

	public SubjectBranchYearMapping getMathBranchMapping(boolean... newValue) {
		if (mathBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			mathBranchMapping = new SubjectBranchYearMapping.SubjectBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setSubject(subjectMath).build();
		}
		return mathBranchMapping;
	}

	public SubjectBranchYearMapping getPhysicsBranchMapping(boolean... newValue) {
		if (physicsBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			physicsBranchMapping = new SubjectBranchYearMapping.SubjectBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setSubject(subjectPhysics).build();
		}
		return physicsBranchMapping;
	}

	public SubjectBranchYearMapping getChemistryBranchMapping(boolean... newValue) {
		if (chemistryBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			chemistryBranchMapping = new SubjectBranchYearMapping.SubjectBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setSubject(subjectChemistry).build();
		}
		return chemistryBranchMapping;
	}

	public SubjectBranchYearMapping getBiologyBranchMapping(boolean... newValue) {
		if (biologyBranchMapping == null || (newValue.length > 0 && newValue[0])) {
			biologyBranchMapping = new SubjectBranchYearMapping.SubjectBranchYearMappingBuilder(null)
					.setBranchYear(branchYear1stCS).setSubject(subjectBiology).build();
		}
		return biologyBranchMapping;
	}

}
