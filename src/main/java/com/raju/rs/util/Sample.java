package com.raju.rs.util;

public class Sample implements AutoCloseable{
	private int i;
	private int j;

	public Sample(int a, int b) {
		this.i = a;
		this.j = b;
	}

	
	public int sum() {
		return i + j;
	}

	public int sub() {
		return i - j;
	}

	public int multiply() {
		return i * j;
	}

	public double divide() {
		return i / j;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}

}